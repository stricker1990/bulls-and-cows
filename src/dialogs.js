const readlineSync=require('readline-sync');

function askDigitsCount(){
    while(true){
        const digitsCount = questionInt('Я умею загадывать числа размером от 3 до 6 цифр. Сколько цифр попробуешь отгадать?');
        if(digitsCount>=3 && digitsCount<=6){
            return digitsCount;
        }
        console.log('Введенное значение должно быть в диапазоне от 3 до 6.');
    }
}

function askUserNumber(digitsCount){
    return readlineSync.question('Попробуй угадать число:', {limitMessage: `Введенное значение должно содержать ${digitsCount} цифр.`, limit: (value)=>{
        return (value.length===digitsCount) && +value!==NaN;
    }});
}

function sayWrongDigets(wrongDigets){
    console.log(`совпавших цифр не на своих местах - ${wrongDigets.outPlaces.length} (${wrongDigets.outPlaces.join(',')}), цифр на своих местах - ${wrongDigets.inPlaces.length} (${wrongDigets.inPlaces.join(',')})`);
}

function qusetionStartNewGame(){
    return readlineSync.keyInYN('Сыграть заново?');
}

function questionInt(questionMessage){
    return readlineSync.questionInt(questionMessage, {
        limitMessage: 'Введенное значение не является числом.'
    });
}

module.exports={
    askDigitsCount,
    askUserNumber,
    sayWrongDigets,
    qusetionStartNewGame
};