function randomNumber(digitsCount){
    const min = Math.pow(10, digitsCount-1);
    const max = Math.pow(10, digitsCount)-1;

    return String(randomFromInterval(min, max));
}

function randomFromInterval(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function findAll(str, subStr, index=-1){

    const result=[];
    do{
        index=str.indexOf(subStr, index+1);
        if(index!==-1){
            result.push(index);
        }
    }while(index>-1);

    return result;
}

function compareNumbers(secretString, userString){

    const result={
        inPlaces: [],
        outPlaces: [],
        isCompare: false
    };

    for(let digit = 0; digit < 10; digit++){
        const symbol = String(digit);
        const count = entersCount(secretString, symbol);
        
        if(count===0){
            continue;
        }

        const outPlaces=[];
        const inPlaces=[];
        const indexes = findAll(userString, symbol);
        indexes.forEach(index => {
            if(secretString[index]===symbol){
                inPlaces.push(symbol);
            }else{
                outPlaces.push(index);
            }
        });

        if(inPlaces.length>0){
            result.inPlaces.push(...inPlaces);
        }

        for(let i=0; i<Math.min(count-inPlaces.length, outPlaces.length); i++){
            result.outPlaces.push(symbol);
        }
    }

    if(result.inPlaces.length===secretString.length){
        result.isCompare=true;
    }

    return result;
}

function entersCount(str, subStr){
    return findAll(str, subStr).length;
}

module.exports={
    randomNumber,
    compareNumbers
};