const {randomNumber, compareNumbers} = require('./src/utils');

const {askDigitsCount, askUserNumber, sayWrongDigets, qusetionStartNewGame} = require('./src/dialogs');


function startNewGame(){
    const tryingCount = 10;

    console.log('Привет, %USERNAME%! Я хочу сыграть с тобой в игру...');
    console.log('Если надоест - нажми CTRL+C');

    const digitsCount=askDigitsCount();

    const secretNumber=randomNumber(digitsCount);

    for(let i=tryingCount; i>0; i--){
        console.log(`У вас осталось ${i} попыток...`);

        const userNumber=askUserNumber(digitsCount);
    
        const result = compareNumbers(secretNumber, userNumber);

        if(result.isCompare){
            console.log(`Ты победил! На этот раз....`);
            console.log(`Я загадал: ${secretNumber}`);
            return;
        }

        sayWrongDigets(result);
    }

    console.log('Ты проиграл! Впрочем, как всегда...');
    console.log(`Я загадал: ${secretNumber}`);
}

while(true) {
    startNewGame();

    if(!qusetionStartNewGame()){
        break;
    }
}
